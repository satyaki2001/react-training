
import './signin.css';

import AppRouter from './AppRouter';
import NoteState from './context/NoteState';

function App() {
  return (

    <NoteState>
    <div className="App">
      <header className="App-header">
        
        <AppRouter></AppRouter>
      </header>
      
    
     
    </div>
    </NoteState>
  );
}

export default App;
