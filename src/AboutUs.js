import { useContext,useEffect } from "react"
import noteContext from "./context/NoteContext";
 // eslint-disable-next-line 
import NoteState from "./context/NoteState";

const AboutUs=()=>{
    const a=useContext(noteContext)
    useEffect(()=>{
        a.update();
    },)
    return(
        <>
        
        <div>Hi.This is about {a.state.name} and he is a {a.state.Role}</div>
        <a href="http://localhost:3000/signin"> Back</a>
        
        </>
    )
}

export default AboutUs;