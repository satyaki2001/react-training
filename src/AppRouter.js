import { BrowserRouter,Routes
,Route
 } from "react-router-dom";
 import Signin from "./signin";
 import AboutUs from "./AboutUs";
 import ContactUs from "./ContactUs";
 export default function AppRouter(){
    return(
        <BrowserRouter>
        <Routes>
            <Route path="/signin" element={<Signin/>} />
            <Route path="/aboutus" element={<AboutUs/>} />
            <Route path="/contactus" element={<ContactUs />} />
            </Routes>
            </BrowserRouter>
    );
 }