import { useState } from "react";
const Signin=()=>{
    const handleClick=()=>{
        console.log("Details submitted");
    }
    const [formData,setFormData]=useState({email:"",password:""});
    const handleChanges=(event)=>{
        console.log(formData);
        setFormData({...formData,[event.target.name]:event.target.value})
    }
    return (
        
        <>
        
        <div class className="container">
            

            <div className="innerContainer">
            <a href="http://localhost:3000/signin"> Sign in</a> 
            <a href="http://localhost:3000/aboutus">   About us</a> 
            <a href="http://localhost:3000/contactus">   Contact us</a>


            
                <p>
                <label>Email Address</label>
                <p>
                <input type="email" placeholder="your email" name="email" onChange={handleChanges
                }></input>
                </p>
                <p>
                <label>Password</label>
                </p>
                <input type="password" placeholder="your password" name="password" onChange={handleChanges}></input>
                <p>
                <button className="submitButton" onClick={handleClick}>Submit</button>
                </p>
                <p>Forgot <a href="www.google.com">password?</a></p>
                </p>
            </div>
        </div>

        </>
    )
}
export default Signin;